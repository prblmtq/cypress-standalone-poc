import '@testing-library/cypress/add-commands';

/**
 * If you add a new global Cypress command, please add it to the
 * <root>/cypress/index.d.ts file as well.
 *
 * We have to extend the namespace so that the TypeScript compiler
 * knows that our new commands exist on the global cypress chainable
 * namespace.
 *
 */
Cypress.Commands.add('login', () => {
  cy.getCookie('user').then((cookie) => {
    if (!cookie?.value) {
      // This is a bogus JWT, taken from jwt.io
      cy.setCookie(
        'user',
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'
      );
    }
  });
});

const commandDelay = Number(Cypress.env('commandDelay') || 0);

/**
 * If a command delay is specified (MILLISECONDS) override some of the default cypress commands.
 * This can be really helpful for both debugging your cypress tests but also can be super helpful
 * when recording your cypress tests and outputting videos. The natural delays (say 300MS or so)
 * make it significantly easier to follow what is going on on the page. This should not be on by default
 * and should always stay at 0 for use in the CI to make sure that the tests run as fast as possible.
 */
if (commandDelay > 0) {
  for (const command of ['visit', 'click', 'trigger', 'type', 'clear', 'reload', 'contains']) {
    Cypress.Commands.overwrite(command, (originalFn, ...args) => {
      return new Promise((r) => setTimeout(() => r(originalFn(...args)), commandDelay));
    });
  }
}
