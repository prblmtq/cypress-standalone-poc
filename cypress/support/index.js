// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your tests files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands';

// Alternatively you can use CommonJS syntax:
// require('./commands')
const codeCoverageSupport = require('@cypress/code-coverage/support');

Cypress.on('window:before:load', (win) => {
  /**
   * Pipe through all browser errors and warnings to the node console.
   *
   * ignore uncaught exceptions since cypress tasks are promises. We're
   * replacing a console.log with an async API log, essentially.
   */
  cy.on('uncaught:exception', () => {
    cy.stub(win.console, 'error').callsFake((msg) => {
      cy.task('error', msg);
    });

    return false;
  });
  cy.on('uncaught:exception', () => {
    cy.stub(win.console, 'warn').callsFake((msg) => {
      cy.task('warn', msg);
    });
    return false;
  });
});
