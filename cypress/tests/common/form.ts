import { Then, When } from 'cypress-cucumber-preprocessor/steps';

When(/the user types '(.*)' into the '(.*)' field/i, (typedValue: string, fieldLabel: string) => {
  cy.get('.MuiFormControl-root').contains(fieldLabel).should('exist').parent().parent().find('input').type(typedValue);
});

Then(/The '(.*)' field should show the '(.*)' error/i, (label: string, message: string) => {
  cy.get('.MuiFormControl-root').contains(label).parent().parent().find('.Mui-error').contains(message);
});

Then(/the '(.*)' field should not have an error/i, (label: string) => {
  cy.get('.MuiFormControl-root').contains(label).parent().parent().should('not.have.descendants', '.Mui-error');
});

Then(/the '(.*)' field value should be '(.*)'/, (label: string, value: string) => {
  cy.get('.MuiFormControl-root')
    .contains(label)
    .should('exist')
    .parent()
    .parent()
    .find('input')
    .should('have.value', value);
});
