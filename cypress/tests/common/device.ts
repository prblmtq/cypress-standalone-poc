import { Given } from 'cypress-cucumber-preprocessor/steps';

export type Device =
  | 'ipad-2'
  | 'ipad-mini'
  | 'iphone-3'
  | 'iphone-4'
  | 'iphone-5'
  | 'iphone-6'
  | 'iphone-6+'
  | 'iphone-7'
  | 'iphone-8'
  | 'iphone-x'
  | 'iphone-xr'
  | 'iphone-se2'
  | 'macbook-11'
  | 'macbook-13'
  | 'macbook-15'
  | 'macbook-16'
  | 'samsung-note9'
  | 'samsung-s10';

export type ViewPort = { width: number; height: number };

export const devices = new Map<Device, ViewPort>([
  ['ipad-2', { width: 768, height: 1024 }],
  ['ipad-mini', { width: 768, height: 1024 }],
  ['iphone-3', { width: 320, height: 480 }],
  ['iphone-4', { width: 320, height: 480 }],
  ['iphone-5', { width: 320, height: 568 }],
  ['iphone-6', { width: 375, height: 667 }],
  ['iphone-6+', { width: 414, height: 736 }],
  ['iphone-7', { width: 375, height: 667 }],
  ['iphone-8', { width: 375, height: 667 }],
  ['iphone-x', { width: 375, height: 812 }],
  ['iphone-xr', { width: 414, height: 896 }],
  ['iphone-se2', { width: 375, height: 667 }],
  ['macbook-11', { width: 1366, height: 768 }],
  ['macbook-13', { width: 1280, height: 800 }],
  ['macbook-15', { width: 1440, height: 900 }],
  ['macbook-16', { width: 1536, height: 960 }],
  ['samsung-note9', { width: 414, height: 846 }],
  ['samsung-s10', { width: 360, height: 760 }],
]);

function setViewPort(viewPort: ViewPort) {
  cy.viewport(viewPort.width, viewPort.height);
}

Given('The user is using a {string}', (name: Device) => {
  const viewPort = devices.get(name);
  if (!viewPort) {
    throw new Error(`No registered device found for key: "${name}"`);
  }
  setViewPort(viewPort);
});

Given(/The user is on a screen with the resolution '(.*)' x '(.*)'/i, (width: string, height: string) => {
  setViewPort({
    width: Number(width),
    height: Number(height),
  });
});
