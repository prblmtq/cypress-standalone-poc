import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps';

When('The user goes to {string}', (url) => {
  cy.visit(url);
});

Then('The user should see the text {string}', (text) => {
  cy.findAllByText(text, { exact: false }).should('be.visible');
});

