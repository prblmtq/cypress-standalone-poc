Feature: An Example

  This is just an example of a cucumber -> cypress test

  Background:
    Given The user is authenticated

  Scenario: As an example, I want to go to google's homepage
    When The user goes to "https://google.com"
    Then The user should see the text "google"