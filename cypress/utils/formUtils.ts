import { Matcher } from '@testing-library/dom';

type DropdownUtilOptions = {
  dropdown?: Cypress.Chainable<JQuery>;
  dropdownLabelText?: Matcher;
  optionValue?: any;
  optionDisplayText?: string;
};

export const openDropdown = (element?: Cypress.Chainable<JQuery>, id?: Matcher) => {
  if (id) {
    element = cy.findAllByLabelText(id);
  }

  if (element) {
    element.first().click();
  } else {
    throw new Error('Dropdown not found or not provided');
  }
};

/** Assumes dropdown is open and focused */
export const selectOption = (optionValue?: any, optionDisplayText?: string) => {
  cy.focused()
    .parent()
    .then((el) => {
      let optionElement;
      if (optionValue) {
        optionElement = el.find(`[data-value="${optionValue}"]`);
      } else if (optionDisplayText) {
        optionElement = el.find(`li:contains("${optionDisplayText}")`).first();
      }

      if (optionElement) {
        optionElement.trigger('click');
      } else {
        throw new Error('Unable to find option');
      }
    });
};

/**
 *
 * @param DropdownUtilOptions: Use either dropdown
 * (if you already have reference) OR dropdownLabelText,
 * and selectValue OR selectDisplayText
 */
export const selectOptionFromDropdown = ({
  dropdown,
  dropdownLabelText,
  optionValue,
  optionDisplayText,
}: DropdownUtilOptions) => {
  openDropdown(dropdown, dropdownLabelText);
  selectOption(optionValue, optionDisplayText);
};
