import 'cypress-localstorage-commands';

export const setLocalStorage = ({ key, value }: { key: string; value: any }) => {
  cy.setLocalStorage(key, value);
};
