// / <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */
const cucumber = require('cypress-cucumber-preprocessor').default;
const codeCoverageTask = require('@cypress/code-coverage/task');
const resolve = require('resolve');

/**
 * This comes installed by default with cypress, we shouldn't install
 * another copy of it because we don't want any unexpected versioning
 * conflicts when we update one or the other.
 */
const browserify = require('@cypress/browserify-preprocessor');

module.exports = (on, config) => {
  on('task', codeCoverageTask(on, config));
  on(
    'file:preprocessor',
    cucumber({
      ...browserify.defaultOptions,
      typescript: resolve.sync('typescript', { baseDir: config.projectRoot }),
    })
  );

  return config;
};
